const mongoose = require( 'mongoose' );
const Store = mongoose.model( 'Store' );

//exports.myMiddleware = ( req, res, next ) => {
	//req.name = "Dan";
	//if ( req.name === "Dan" ) {
		//throw Error( "That is a stupid name!" );
	//}
	//next();
//};

exports.homePage = ( req, res ) => {
	console.log( req.name );
	//Example Flashes:
	//req.flash( 'error', 'Something happened ...' );
	//req.flash( 'warning', 'Something happened ...' );
	//req.flash( 'info', 'Something happened ...' );
	//req.flash( 'success', 'Something happened ...' );
	res.render( 'index' )
};

exports.addStore = ( req, res ) => {
	res.render( 'editStore', { title: 'Add Store' } );
};

exports.createStore = async ( req, res ) => {
	//res.json( req.body );
	const store = await ( new Store( req.body ) ).save();
	//await store.save();
	req.flash( 'success', `Successfully Created ${ store.name }. Care to leave a review?` );
	res.redirect( `/store/${ store.slug }` );
};

exports.getStores = async ( req, res ) => {
	// Query the DB for a list of all stores
	const stores = await Store.find();
	//console.log( store );
	//res.render( 'stores', { title: 'Stores', stores: stores } );
	res.render( 'stores', { title: 'Stores', stores } );
};

exports.editStore = async ( req, res ) => {
	// 1. Find the store given the ID
	const store = await Store.findOne( { _id: req.params.id } );
	// 2. Confirm they are the owner of the store
	// TODO
	// 3. Render out the edit form so the user can update their store
	//res.render( 'editStore', { title: `Edit ${ store.name }`, store: store } );
	res.render( 'editStore', { title: `Edit ${ store.name }`, store } );
}

exports.updateStore = async ( req, res ) => {
	// 1. Set the location data to be a point
	req.body.location.type = 'Point';
	// 2. Find and update the store
	const store = await Store.findOneAndUpdate( { _id: req.params.id }, req.body, {
		new: true, // returns new store instead of old one
		runValidators: true // force model to run reduired validators on update (by default only runs on create)
	} ).exec(); // exec will run the query - not all run by default
	// 3. Redirect to the store and notify success
	req.flash( 'success', `Successfully updated <strong>${ store.name }</strong>. <a href="/stores/${ store.slug }">View Store »</a>` )
	res.redirect(`/stores/${ store._id }/edit`);
}
